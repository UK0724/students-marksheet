const { v4: uuidv4 } = require('uuid')
let students = require('../students')

const getStudents = (req, reply) => {
  reply.send(students)
}

const getStudent = (req, reply) => {
  const { id } = req.params

  const student = students.find((item) => item.id === id)

  reply.send(student)
}

const addStudent = (req, reply) => {
  const { name ,subject1 ,subject2 ,subject3 ,subject4, subject5} = req.body

  console.log(req.body);

  const student = {
    id: uuidv4(),
    name,
    subject1,
    subject2,
    subject3,
    subject4,
    subject5
  }

  students = [...students, student]

  reply.code(201).send(student)
}

const deleteStudent = (req, reply) => {
  const { id } = req.params

  students = students.filter((student) => student.id !== id)

  reply.send({ message: `student ${id} has been removed` })
}

const updateStudent = (req, reply) => {
  const { id } = req.params
  const { name ,subject1 ,subject2 ,subject3 ,subject4, subject5} = req.body

  students = students.map((student) => (student.id === id ? { id, name, subject1, subject2, subject3, subject4, subject5} : student))

  let student = students.find((student) => student.id === id)

  reply.send(student)
}

module.exports = {
  getStudents,
  getStudent,
  addStudent,
  deleteStudent,
  updateStudent,
}
