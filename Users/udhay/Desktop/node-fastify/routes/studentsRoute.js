const {
  getStudents,
  getStudent,
  addStudent,
  deleteStudent,
  updateStudent,
} = require("../controllers/student.js");

// student schema
const Student = {
  type: "object",
  properties: {
    id: { type: "string" },
    name: { type: "string" },
    subject1: { type: "number" },
    subject2: { type: "number" },
    subject3: { type: "number" },
    subject4: { type: "number" },
    subject5: { type: "number" },
  },
};

// Options for get all items
const getStudentsOpts = {
  schema: {
    response: {
      200: {
        type: "array",
        students: Student,
      },
    },
  },
  handler: getStudents,
};

const getStudentOpts = {
  schema: {
    response: {
      200: Student,
    },
  },
  handler:   getStudent ,
};

const postStudentOpts = {
  schema: {
    body: {
      type: "object",
      required: ["name","subject1","subject2","subject3","subject4","subject5"],
      properties: {
        name: { type: "string" },
        subject1: { type: "number" },
        subject2: { type: "number" },
        subject3: { type: "number" },
        subject4: { type: "number" },
        subject5: { type: "number" },
      },
    },
    response: {
      201: Student,
    },
  },
  handler: addStudent,
};

const deleteStudentOpts = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
  handler: deleteStudent,
};

const updateStudentOpts = {
  body: {
    type: "object",
    required: ["name","subject1","subject2","subject3","subject4","subject5"],
    properties: {
      name: { type: "string" },
      subject1: { type: "number" },
      subject2: { type: "number" },
      subject3: { type: "number" },
      subject4: { type: "number" },
      subject5: { type: "number" },
    },
  },
  schema: {
    response: {
      200: Student,
    },
  },
  handler: updateStudent,
};

function itemRoutes(fastify, options, done) {
  // Get all Students
  fastify.get("/students", getStudentsOpts);

  // Get single Student
  fastify.get("/students/:id", getStudentOpts);

  // Add Student
  fastify.post("/students", postStudentOpts);

  // Delete Student
  fastify.delete("/students/:id", deleteStudentOpts);

  // Update Student
  fastify.put("/students/:id", updateStudentOpts);

  done();
}

module.exports = itemRoutes;
